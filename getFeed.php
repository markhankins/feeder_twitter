<?php
require('vendor/autoload.php');
include('includes/feeds/TwitterFeed.php');

$basepath = 'feeder.local:8080/getFeed?account=';
$account = $_GET['account'];

function getTweets($account, $retrieve = 10, $show = 6, $date_format = 'D h:i'){
    $options = [
        'twitter_screen_name'   => $account,
        'tweets_to_retrieve'    => 6,
        'ignore_replies'        => true,
        'ignore_retweets'       => false,
    ];
    $twitter = new TweetPHP($options);
    $tweets = $twitter->get_tweet_array();

    $output = '';
    foreach($tweets as $tk=>$tv){
        $time = date($date_format,strtotime($tv['created_at']));
        $output.='<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
    }

    return $output;
}

$output = getTweets($account);
echo $output;


