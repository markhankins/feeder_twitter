<?php
$date_format = ('D h:i');

/**
 * Feeds to display will include
 * 1.Metro
 * 2.Tyne Tunnel
 * 3.Traffic
 * 4.Weather
 * 5.What's on
 * 6.Cobalt exchange
 * 7.CBX Offers
 */
require('vendor/autoload.php');
//include('includes/feeds/TwitterFeed.php')
?>
<!DOCTYPE html>
<html>
<?php
    include('includes/header.php');
?>
<body>
<?php
    include('includes/nav.php');
?>
<h1>The Craic@Cobalt</h1>
<div class="section group">
    <div class="col span_1_of_5" id="twitter">
        <h2>Twitter</h2>
        <p>@Accenture</p>
        <div class="inner"></div>

        <?php
        //get the initial content
        $options = [
            'twitter_screen_name'   => 'Accenture',
            'tweets_to_retrieve'    => 6,
            'ignore_replies'        => true,
            'ignore_retweets'       => false,
        ];
        $twitter = new TweetPHP($options);
        $tweets = $twitter->get_tweet_array();

        foreach($tweets as $tk=>$tv){
            $time = date($date_format,strtotime($tv['created_at']));
            echo '<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
        }
        ?>
    </div>
    <div class="col span_1_of_5" id="weather">
        <h2>Weather</h2>
        <p>@Newcast_Weather</p>

        <div class="inner"></div>

        <?php
        $options = [
            'twitter_screen_name'   => 'Newcast_Weather',
            'tweets_to_retrieve'    => 6,
            'ignore_replies'        => true,
            'ignore_retweets'       => false,
        ];
        $twitter = new TweetPHP($options);
        $tweets = $twitter->get_tweet_array();
        foreach($tweets as $tk=>$tv){
            $time = date($date_format,strtotime($tv['created_at']));
            echo '<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
        }
        ?>
    </div>
    <div class="col span_1_of_5" id="traffic">
        <h2>Traffic</h2>
        <p>@netrafficnews</p>

        <div class="inner"></div>
        <?php
        $options = [
            'twitter_screen_name'   => 'netrafficnews',
            'tweets_to_retrieve'    => 6,
            'ignore_replies'        => true,
            'ignore_retweets'       => true,
        ];
        $twitter = new TweetPHP($options);
        $tweets = $twitter->get_tweet_array();
        foreach($tweets as $tk=>$tv){
            $time = date($date_format,strtotime($tv['created_at']));
            echo '<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
        }
        ?>
    </div>
    <div class="col span_1_of_5" id="tynetunnel">
        <h2>Tyne Tunnel</h2>
        <p>@tt2limited</p>

        <div class="inner"></div>

        <?php
        $options = [
            'twitter_screen_name'   => 'tt2limited',
            'tweets_to_retrieve'    => 10,
            'ignore_replies'        => false,
            'ignore_retweets'       => false,
        ];
        $twitter = new TweetPHP($options);
        $tweets = $twitter->get_tweet_array();
        foreach($tweets as $tk=>$tv){
            $time = date($date_format,strtotime($tv['created_at']));
            echo '<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
        }
        ?>
    </div>
    <div class="col span_1_of_5" id="metro">
        <h2>Metro</h2>
        <p>@my_metro</p>

        <div class="inner"></div>

        <?php
        $options = [
            'twitter_screen_name'   => 'my_metro',
            'tweets_to_retrieve'    => 10,
            'ignore_replies'        => true,
            'ignore_retweets'       => true,
        ];
        $twitter = new TweetPHP($options);
        $tweets = $twitter->get_tweet_array();
        foreach($tweets as $tk=>$tv){
            $time = date($date_format,strtotime($tv['created_at']));
            echo '<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
        }
        ?>
    </div>
    <!--div class="col span_1_of_8" id="whatson">
        <h2>What's On</h2>
        <p>@whatsonne</p>

        <div class="inner"></div>


        <?php
        /*$options = [
            'twitter_screen_name'   => 'whatsonne',
            'tweets_to_retrieve'    => 6,
            'ignore_replies'        => true,
            'ignore_retweets'       => true,
        ];
        $twitter = new TweetPHP($options);
        $tweets = $twitter->get_tweet_array();
        foreach($tweets as $tk=>$tv){
            $time = date($date_format,strtotime($tv['created_at']));
            echo '<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
        }*/
        ?>
    </div-->
    <!--div class="col span_1_of_8" id="cobalt">
        <h2>Cobalt</h2>
        <p>@CobaltPark</p>

        <div class="inner"></div>

        <?php
        /*
        $options = [
            'twitter_screen_name'   => 'CobaltPark',
            'tweets_to_retrieve'    => 20,
            'ignore_replies'        => true,
            'ignore_retweets'       => true,
        ];
        $twitter = new TweetPHP($options);
        $tweets = $twitter->get_tweet_array();
        foreach($tweets as $tk=>$tv){
            $time = date($date_format,strtotime($tv['created_at']));
            echo '<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
        }*/
        ?>
    </div-->
    <!--div class="col span_1_of_8" id="offers">
        <h2>Offers</h2>
        <p>@morecobalt</p>

        <div class="inner"></div>

        <?php
        /*$options = [
            'twitter_screen_name'   => 'morecobalt',
            'tweets_to_retrieve'    => 20,
            'ignore_replies'        => true,
            'ignore_retweets'       => true,
        ];
        $twitter = new TweetPHP($options);
        $tweets = $twitter->get_tweet_array();
        foreach($tweets as $tk=>$tv){
            $time = date($date_format,strtotime($tv['created_at']));
            echo '<div class="tweet">'.$tv['text'].'<span class="time">'.$time.'</span></div>';
        }*/
        ?>
    </div-->
</div>
<?php
    include('includes/footer.php');
    include('includes/scripts.php');
?>
</body>
</html>
