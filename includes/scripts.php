<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var feedUrl = 'http://feeder.local:8081/getFeed.php?account=';//base path to hit
        var feed = 'Accenture';//twitter feed to fetch
        var timeout = 60;//defult timeout

        function loadFeed(feed, target, reload) {
            reload = reload * 1000;//convert to microtime
            $(target).empty();
            var content = $(target).load(feedUrl+feed, function () {});
            $(target).html(content);
            //console.log('first load, reload in '+reload/1000+' seconds.');
            setInterval(function () {
                //console.log('reloaded');
                $(target).fadeOut('slow', function() {
                    content = $(target).load(feedUrl+feed, function () {});
                    $(target).html(content);
                    $(target).fadeIn('slow');
                });
            }, reload);
        }

        loadFeed('Accenture', '#twitter .inner', 60);//feed name, target, reload
        loadFeed('Newcast_Weather', '#weather .inner', 60);//feed name, target, reload
        loadFeed('netrafficnews', '#traffic .inner', 60);//feed name, target, reload
        loadFeed('tt2limited', '#tynetunnel .inner', 60);//feed name, target, reload
        loadFeed('my_metro', '#metro .inner', 60);//feed name, target, reload
        loadFeed('whatsonne', '#whatson .inner', 60);//feed name, target, reload
        loadFeed('CobaltPark', '#cobalt .inner', 60);//feed name, target, reload
        loadFeed('morecobalt', '#offers .inner', 60);//feed name, target, reload

        //$('#twitter .inner').load('http://feeder.local:8081/getFeed.php?account=Accenture');
        //$('#weather .inner').load('http://feeder.local:8081/getFeed.php?account=Newcast_Weather');
        //$('#traffic .inner').load('http://feeder.local:8081/getFeed.php?account=netrafficnews');
        //$('#tynetunnel .inner').load('http://feeder.local:8081/getFeed.php?account=tt2limited');
        //$('#metro .inner').load('http://feeder.local:8081/getFeed.php?account=my_metro');
        //$('#whatson .inner').load('http://feeder.local:8081/getFeed.php?account=whatsonne');
        //$('#cobalt .inner').load('http://feeder.local:8081/getFeed.php?account=CobaltPark');
        //$('#offers .inner').load('http://feeder.local:8081/getFeed.php?account=morecobalt');
    });
</script>